***Settings***
Library  Builtin

***Variables***
${pie}=  3.14
${r}=  10
${Area}=  0
 
***Test Cases***
1. คำนวณพื้นที่วงกลม
    ${Area}=  squareArea  ${pie}  ${r}
    set Global Variable  ${Area}

2. แสดงผล
    Log To Console  \nArea: ${Area}


***Keywords***
squareArea
    [Arguments]  ${paraPie}  ${paraR}
    ${respArea}=  Evaluate  ${paraPie} * ${paraR} * ${paraR}
    [Return]  ${respArea}